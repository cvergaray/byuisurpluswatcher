﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Eventing.Reader;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using HtmlAgilityPack;

namespace ByuiSurplusParser {
	public class SurplusItem {
		public int ItemNumber { get; set; }
		public int Qty { get; set; }
		public string Description { get; set; }
		public string Condition { get; set; }
		public string Price { get; set; }
		public string Unit { get; set; }

		private bool _quick;
		public string Quick
		{
			get { return _quick ? "Quick" : "Bid"; }
			set { _quick = value == "Quick"; }
		}

		public SurplusItem()
		{
			
		}

		public SurplusItem(IReadOnlyList<string> itemValues)
		{
			try
			{
				ItemNumber = int.Parse(itemValues[0]);
				Qty = int.Parse(itemValues[1]);
				Description = itemValues[2];
				Condition = itemValues[3];
				Price = itemValues[4];
				Unit = itemValues[5];
				Quick = itemValues[6];
			}
			catch (Exception)
			{
				ItemNumber = 0;
				Qty = 0;
			}
		}

		public string Stringify()
		{
			return $"{ItemNumber.ToString()}\t{Qty.ToString()}\t{Description}\t{Condition}\t{Price}\t{Unit}\t{Quick}";
		}

		public static List<List<string>> GetSurplusItemsAsStrings() {
			var webClient = new WebClient();
			var page = webClient.DownloadString("http://dep.byui.edu/SurplusList/");

			var doc = new HtmlAgilityPack.HtmlDocument();
			doc.LoadHtml(page);

			var table = doc.DocumentNode.SelectSingleNode("//table[@class='GridViewDefault']")
						.Descendants("tr");
			var htmlNodes = table as IList<HtmlNode> ?? table.ToList();
			var filteredTable = htmlNodes
						.Skip(1)
						.Except(new HtmlNode[] { htmlNodes.Last()})
						.Where(tr => tr.Elements("td").Count() > 1)
						.Select(tr => tr.Elements("td").Select(td => td.InnerText.Trim()).ToList())
//						.Select(tr => new SurplusItem(tr.Elements("td").Select(td => td.InnerText.Trim()).ToList()))
						.ToList();
			return filteredTable;
		}

		public bool Equals(SurplusItem obj)
		{
			return this.ItemNumber == obj.ItemNumber;
		}

		public static List<SurplusItem> GetSurplusItems()
		{
			return GetSurplusItemsAsStrings().Select(item => new SurplusItem(item)).ToList();
		}
	}
}
