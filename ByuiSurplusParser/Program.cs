﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Web;
using HtmlAgilityPack;
using PushoverNotifier;

namespace ByuiSurplusParser {
	public class Program {
		public static void Main(string[] args)
		{
			try
			{
//				LogText(@"Checking for new items");
				var message = CheckForNewItems();
				if (message != null) LogText(message);
//				LogText("\n\n===========================Application Execution Completed===============================\n\n");
			}
			catch (Exception e)
			{
				try
				{
					LogText($"Error occured: {e.Message}");
				}
				catch (Exception error)
				{
					Notifier.SendMessage($"Error Logging Error Message.\nInner: {error.Message}\nOriginal:{e.Message}");
				}
				throw;
			}
		}

		public static string CheckForNewItems() {
			var postedList = SurplusItem.GetSurplusItems();
			var currentList = FileDriver.GetItemListAsSurplusItems();
			var newItems = (from posted in postedList let contains = currentList.Any(posted.Equals) where !contains select posted).ToList();
			currentList.AddRange(newItems);
			FileDriver.SaveItemList(postedList);
			var message = $"{newItems.Count} New Surplus Items";
			message = newItems//.Where(newItem => newItem.Description.ToLowerInvariant().Contains("yamaha"))
				.Aggregate(message, (current, newItem) => $"{current}\n{newItem.Description}\nCost: {newItem.Price}\nItem #{newItem.ItemNumber}");
			if (newItems.Count > 0)
				Notifier.SendMessage(HttpUtility.HtmlDecode(message));
			else return null;
			return message;
		}

		public static void LogText(string message)
		{
			Console.WriteLine(message);
			FileDriver.AppendToFile($"{DateTime.Now.ToString("s")} -- {message}\n", "log.txt");
		}


		public static List<Dictionary<string, string>> GetTest()
		{
			var webClient = new WebClient();
			var page = webClient.DownloadString("http://grntweb.gresham.lsil.com/EngToolbox/ieDefault.html");

			var doc = new HtmlAgilityPack.HtmlDocument();
			doc.LoadHtml(page);

			var table = doc.DocumentNode
				.Descendants("a");
			var htmlNodes = table as IList<HtmlNode> ?? table.ToList();
			var filteredTable = htmlNodes
				.Select(
					tr => new Dictionary<string, string> {{"Link Name", tr.InnerHtml}, {"href", tr.GetAttributeValue("href", "n/a")} })
				.ToList();

			foreach (var link in filteredTable)
			{
				var appname = "";
				if (link["href"].ToLowerInvariant().Contains("grexmaweb"))
				{
					var tokenized = link["href"].ToLowerInvariant().Split('/');
					for (var i = 0; i < tokenized.Length - 1; i++)
					{
						if (tokenized[i] == "webapps")
						{
							appname = tokenized[i + 1];
						}
					}
				}
				link.Add("AppName", appname);
			}
			return filteredTable;
		}
	}
}
