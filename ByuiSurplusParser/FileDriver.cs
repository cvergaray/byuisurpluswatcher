﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Web.Script.Serialization;

namespace ByuiSurplusParser {
	public class FileDriver
	{
		internal const string AllItemsName = "AllItemsList.txt";
		internal const string AppDataFolder = "ByuiParserData\\";

		public static string ReadFile(string fileName, string directory = null)
		{
			var fullPath = (directory ?? AppDomain.CurrentDomain.BaseDirectory + AppDataFolder + fileName);
			return File.Exists(fullPath) ? File.ReadAllText(fullPath) : "";
		}
		public static void WriteToFile(string data, string fileName, string directory = null)
		{
			var fullPath = directory ?? AppDomain.CurrentDomain.BaseDirectory + AppDataFolder + fileName;
			(new FileInfo(fullPath)).Directory?.Create();
			File.WriteAllText(fullPath, data);
		}

		public static void AppendToFile(string data, string fileName, string directory = null)
		{
			var fullPath = PrepareUri(directory, fileName);
			File.AppendAllText(fullPath, data);
		}

		public static string PrepareUri(string directory, string fileName)
		{
			var fullPath = directory ?? AppDomain.CurrentDomain.BaseDirectory + AppDataFolder + fileName;
			(new FileInfo(fullPath)).Directory?.Create();
			return fullPath;
		}

		public static void SaveItemList(object allItems) {
			var serialized = new JavaScriptSerializer().Serialize(allItems);
			WriteToFile(serialized, AllItemsName);
		}


		public static List<List<string>> GetItemListAsStrings()
		{
			var serialized = ReadFile(AllItemsName);
			var deserialized = new JavaScriptSerializer().Deserialize<List<List<string>>>(serialized);
			return deserialized ?? new List<List<string>>();
		}

		public static List<SurplusItem> GetItemListAsSurplusItems() {
			var serialized = ReadFile(AllItemsName);
			var deserialized = new JavaScriptSerializer().Deserialize<List<SurplusItem>>(serialized);
			return deserialized ?? new List<SurplusItem>();
		}



	}
}