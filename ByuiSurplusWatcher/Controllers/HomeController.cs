﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ByuiSurplusParser;
using ByuiSurplusWatcher.Models;
using PushoverNotifier;

namespace ByuiSurplusWatcher.Controllers {
	public class HomeController : Controller {
		public ActionResult Index()
		{
			SurplusItemModel.CheckForNewItems();
			return View();
		}

		public ActionResult About() {
			ViewBag.Message = "Your application description page.";

			return View();
		}

		public ActionResult Contact() {
			ViewBag.Message = "Your contact page.";

			return View();
		}
	}
}