﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Web.Script.Serialization;
using ByuiSurplusParser;

namespace ByuiSurplusWatcher.Models {
	public class FileDriverModel
	{
		internal const string AllItemsName = "AllItemsList.txt";

		public static string ReadFile(string fileName, string directory = null)
		{
			var fullPath = (directory ?? AppDomain.CurrentDomain.GetData("DataDirectory")) + "//" + fileName;
			return File.Exists(fullPath) ? File.ReadAllText(fullPath) : "";
		}
		public static void WriteToFile(string data, string fileName, string directory = null) {
			var fullPath = directory ?? AppDomain.CurrentDomain.GetData("DataDirectory") + "//" + fileName;
			File.WriteAllText(fullPath, data);
		}

		public static void SaveItemList(object allItems) {
			var serialized = new JavaScriptSerializer().Serialize(allItems);
			WriteToFile(serialized, AllItemsName);
		}


		public static List<List<string>> GetItemListAsStrings()
		{
			var serialized = ReadFile(AllItemsName);
			var deserialized = new JavaScriptSerializer().Deserialize<List<List<string>>>(serialized);
			return deserialized ?? new List<List<string>>();
		}

		public static List<SurplusItem> GetItemListAsSurplusItems() {
			var serialized = ReadFile(AllItemsName);
			var deserialized = new JavaScriptSerializer().Deserialize<List<SurplusItem>>(serialized);
			return deserialized ?? new List<SurplusItem>();
		}



	}
}