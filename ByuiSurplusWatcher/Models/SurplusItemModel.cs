﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PushoverNotifier;

namespace ByuiSurplusWatcher.Models {
	public class SurplusItemModel
	{
		public int ItemNumber { get; set; }
		public string Description { get; set; }
		public int Quantity { get; set; }
		public int Price { get; set; }
		public bool Quick { get; set; }
		public string Unit { get; set; }
		public ConditionModel Condition { get; set; }

		public SurplusItemModel()
		{
			
		}

		public SurplusItemModel(IReadOnlyList<string> itemValues) {
			try {
				ItemNumber = int.Parse(itemValues[0]);
				Quantity = int.Parse(itemValues[1]);
				Description = itemValues[2];
				Condition = new ConditionModel(itemValues[3]);
				Price = int.Parse(itemValues[4].Replace("$", "").Replace(".", ""));
				Unit = itemValues[5];
				Quick = itemValues[6] == "QUICK";
			} catch (Exception) {
				//ignored
			}
		}

		public static void CheckForNewItems()
		{
			var postedList = ByuiSurplusParser.SurplusItem.GetSurplusItems();
			var currentList = FileDriverModel.GetItemListAsSurplusItems();
			var newItems = (from posted in postedList let contains = currentList.Any(posted.Equals) where !contains select posted).ToList();
			currentList.AddRange(newItems);
			FileDriverModel.SaveItemList(currentList);
			var message = $"{newItems.Count} New Surplus Items";
			message = newItems//.Where(newItem => newItem.Description.ToLowerInvariant().Contains("yamaha"))
				.Aggregate(message, (current, newItem) => $"{current}\n{newItem.Description}\nCost: {newItem.Price}\nItem #{newItem.ItemNumber}");
			Notifier.SendMessage(message);
		}
	}

	public class ConditionModel
	{
		public int Id { get; set; }
		public string Name { get; set; }

		public ConditionModel(string name)
		{
			switch (name)
			{
				case "POOR":
					Id = 1;
					break;
				case "GOOD":
					Id = 2;
					break;
				case "FAIR":
					Id = 3;
					break;
				case "EXCELLENT":
					Id = 4;
					break;
				default:
					Id = 0;
					break;
			}
			Name = name;
		}
	}
}